# The Techno-Humanist Manifesto

This is a response to the [Techno-Optimist Manifesto](https://a16z.com/the-techno-optimist-manifesto/) by Andreesen Horowitz.

## Lies

We are being lied to.

We are being told that what can be measured is important. And that what cannot be measured is unimportant. 

What follows from that, we are told, is that which increases *some metric* is good, and that which decreases *some metric* is bad. We are being told that we must seek to optimize that which can be measured.

That the best students get the best grades.

That the best scientists publish the most papers, which get the most citations.

That the most important websites get the most pageviews.

That the best ideas sell the most books, or get the most retweets and likes.

That the businesses and individuals which contribute the most to society are rewarded with the most money.

## Truth

Humans have always pursued a wide array of different lives and different goals. What makes a life well lived has been the subject of writing and debate for millenia. Some things people consider meaningful include:

- enjoying rich experiences, including travel, leisure, food, works of art
- creating things or ideas for the world to use or enjoy
- caring for children, the elderly, and those who otherwise cannot fully care for themselves
- sharing our lives with friends and those we love
- striving to make our society, country or world a better place
- finding wisdom in experience itself, especially in those experiences which are unpleasant or painful

Sometimes the relative success and failure of those goals is something that can be easily measured, and sometimes it is not. More often, a measurement can act as a proxy for relative success. And these are useful, as long we can remember that they are only proxies.

A granting agency must know where to spend its money. An investor must know what to invest in. A medical school must know which students to admit. A non-profit striving to make the world better must know what "better" means. And so they must use metrics as proxies to decide.

However, we are forgetting that the proxies are proxies. And so we optimize our work, our organizations, our lives, and ourselves to survive within an environment where we only care about what can be measured. We discard the richness of possible lives, goals, paths, and experiences in the pursuit of optimization.

## Technology

Since the first stone tools, humans have sought to make our lives better through innovation.

For the most part, technological progress is an unabashed good. Vaccines, fertilizer, automobiles, and birth control, among others, have done more to further human well-being than any other endeavour we have set our minds to. No one who proposes going backwards and living without any of these things has thought seriously about the scale of human suffering that would be caused (even if going backwards were at all possible).

Technology is produced by science, the investigation of the rules of the universe and planet we inhabit. Our knowledge of these rules is incomplete, otherwise technological progress would already have ceased. We do not know what science will discover in the future, and we do not know what technologies will be invented in the future that change our lives or how it will change them.

Thus far, science has produced exactly one technology with the capacity to end the existence of our species. By sheer accident of the rules of the universe, this doomsday scenario is relatively difficult to execute in practice, requiring the collective efforts and resources of at least a nation-state. There may be other traps lurking in the rules of science, and once discovered they cannot be undiscovered. 

Thus we must always proceed with extreme caution: no matter how much human wellbeing has been increased by technology thus far, nothing in the rules we have found so far says this will always be so.

We believe that the purpose of technology is to serve humankind. We do not believe that technology itself has "rights". Its existence and adoption is justifiable only to the extent to which it makes the lives of people better. If it does not do so, if it indeed makes peoples' lives worse, then we have the right to use our collective action, our government, and our institutions to regulate it, to abolish it, and to fight it.

## Markets

Like technology, markets are overall a powerful force for good in the world.

The ability to exchange goods and services allowed humans to specialize in crafts and endeavours other than constant subsistence, freeing us time to create better solutions to our problems that in turn drove further exchange and specialization. Money, at its root, is merely an extension of this basic practice, which makes exchange far more flexible in time and space.

However, exchange also creates opportunities for abuse. Where there are conduits of exchange, there are opportunities to occupy a conduit's chokepoint, and skiff off outsized amounts of wealth from those buying and selling goods and services. This is a major source of inequality, because while each of us has the same in-born ability to produce, those who profit from their efforts without putting in effort themselves merely gain more wealth with which to obtain more chokepoints.

It is also notable that while exchange is by definition freely given by two parties, and therefore should be mutually beneficial, this is only true insofar as both parties have sufficient information about the goods and services being exchanged, and about the behaviour of the broader market. Therefore, the principle of exchange can be abused by those who possess disproportionate information about the market. This creates perverse incentives for those who acquire, exchange, and judiciously distribute such information.

Markets must be regulated if they are to work for the entire population, not just for those who already occupy positions of power. They must be regulated to prevent the outsized accumulation of chokepoints, and to ensure that information about the behaviour of the market is justly and evenly distributed.

## Knowledge

Technological progress has waxed and waned at various periods throughout history. Some areas of the world made discoveries no one else did; some periods accelerated faster than others. However, we have been on a collective unbroken winning streak for several hundred years.

This winning streak traces its origins to Johann Gutenberg's printing press. The democratization of publishing led to a flourishing of ideas variously called the Renaissance, the Enlightenment, the Reformation, or the scientific revolution, depending on the area of human endeavour under discussion. *Every* area of human endeavour underwent this transformation at once.

It wasn't all good. As power strutures came loose, brutal conflict was often the immediate result. Yet the ensuing marketplace of ideas created a world where *nearly everyone* had greater capacity for flourishing, and the gap in capacity for flourishing between the most and least fortunate was *greatly* reduced.

The internet has unleashed a kind of fast-motion replay of the destabilization that followed the advent of the printing press. The massive decrease in the ease with which information can be published, trafficked, stored, and consumed, has created an ecosystem where it can be hard to know what to trust. Even worse, malicious parties have exploited that breakdown in trust for their own nefarious purposes. 

The exponential increase in information flow has utterly transformed how goods and services are brought to market. New chokepoints in the exchange landscape have sprung up so quickly that they were occupied and exploited by would-be abusers before most people even realized what was happening.

But the most disorienting part of the information revolution is the gradual realization that the internet is a book that reads *us*. We use the internet to get information about the world, but in exchange, the world gets information about us. While the printing press merely unbound the power structures of the medieval world, the internet appears to be more like a double-edged sword: it allows us to speak truth to power, while simultaneously creating new mechanisms of control.

This chapter of human history is still being written. We do not know if we will eventually inoculate ourselves against those who seek to exploit information abundance, or if we will enter a new dark age in which information is so abundant that it becomes increasingly worthless. To survive it, we must champion a new set of fundamental human values, that seek to share the benefits of information abundance more equally among all human beings.

We believe in objective truth, and that the passion of the masses cannot override humble reason and observation. While we each may have our own perspective, the goal of conversation is to understand the world better, not to win debates or humiliate our opponents.

We believe that those who seek to understand the world, whether scholars, scientists, or journalists, are doing work of immesurable value and should *be* valued, even when we do not like what they have to say.

We believe that open protocols and radical interoperability will limit the power of those who exploit chokepoints. We believe that government and institutions should have the power to destroy information chokepoints through regulation, when it can be determined that it would be in the public interest to do so.

We believe that all human beings have a certain inaliable right to privacy, rights which *cannot* be taken from them, even if they explicitly sign them away.

## Intelligence

Technological change by its very nature means that machines can do many things that humans once had a monopoly on. This means that the nature of human endeavour must change as technology becomes more capable of doing certain tasks. For a long time, this mostly applied to various kinds of physical labour. Now, machines appear to be able to do more and more mental tasks as well.

We do not know how the new technological revolution of machine learning will pan out. Like all technology, the potential to increase human flourishing is real. No doubt, it will change many kinds of work, and we must adapt with it. We do not believe it would be ethical to go backwards, even if it were possible. 

We do believe that the law and insitutions must adapt in several important ways, because of three basic truths about machine learning that proponents tend to omit.

Firstly, while the nature of technological change is to make certain kinds of work obsolete, the rate of change is now so fast that people cannot be held wholly responsible for developing skills that are no longer needed. (In fact, we believe that the cultivation of skill is something of value in its own right.) We also believe that the welfare state should be expanded to care for those who have found once marketable trades to be no longer so.

Secondly, machine learning is (oftentimes) a deception, and another form of exploitation wrought by the information revolution. Whether computers really "think" is a matter for philosophers, but the simple fact is that all current implementations of machine learning depend on vast amounts of human-produced training data. Nearly all of this data is collected without the explicit consent of those providing it. Thus, the *appearance* of a machine thinking is often a delusion, whereby human-thinking is appropriated, masked by a model, and passed off as "machine intelligence".

Thirdly, machine intelligence *always* differs from human intelligence in one critical respect: it depends on optimizing towards some goal. A human decision-maker can consider a broad spectrum of possible human values that may stem from choosing between an array of options. These broader considerations may as well be called *ethics*. A machine decision-maker *must* make a decision that optimizes some along predetermined parameter.

We believe it is impossible for machine intelligence to be ethical, even when it is programmed with the best of intentions.

## Finitude

Everyone who has ever lived, has lived on a sphere 0.04 light **seconds** in diameter. A handful of people have traveled as much as 1.28 light **seconds** away.

The nearest distance at which we *might* find another planet capable of sustaining us is 4.4 light **years**. The magnitude of this difference is incomprehensible to our minds. Even with exponential technological progress, it may well be that the stars will be forever out of reach.

Our planet has finite resources. We have limited land upon which to live and to grow food. We have limited surface metals and fossil fuels. Most importantly, we have a limited capacity in terms of land, air, and water to absorb the waste-products of our civilization.

We have a finite capacity, but our population, economy, and resource use continue to grow exponentially.

Technology itself is our best hope to live in harmony with the planet. More advanced societies have smaller families, and exponential population growth is slowing. Technology may help us better utilize the resources we *do* need, and to manage the waste we *do* produce. The future is unwritten, and we believe we may yet get there.

We have no reason to think, however, that exponential growth can proceed forever in a system which is limited. Such a mindset boggles common sense, and everything science has learned about the natural world. 

To oppose sustainability is exactly the same thing as supporting catastrophe.

## Opposition

We have opponents, not enemies. We believe in the fundamental brotherhood of all people. We believe that our destiny is largely shared, that progress will lift all boats, and that catastrophe will be borne by us all. With technology as the servent of humanity, our hope is that humans living in the distant future will enjoy a kind of flourishing far above our own, just as we enjoy a flourishing far beyond humans of the ancient world.

Nevertheless, to realize this goal, we must keep in mind that technology, markets, money, the internet and machine learning are human institutions and should always be subservient to all people.

Our opponents, in contrast, claim that technology and the free market are akin to laws of nature, and that resisting their *natural* progression is either futile, or actively harmful. This mindset goes by many names: neoliberalism, techno-optimism, techno-libertarianism, trickle-down economics, meritocracy, and the sociopathic phrase "move fast and break things".

## Collective action

The most pernicious effect of the techno-optimist mindset is the extent to which we have absorbed the idea that our collective efforts are worthless.

When we are taught that our efforts only have value to the extent that they can be monitized, we forget that *enough* people, acting in unity, can have tremendous power.

All insitutions are like the emperor with no clothes - the illusion only lasts as long as we all keep pretending. We pretend because we mistakenly believe we can win a game that was always stacked against us.

It's time to cast aside the lie. It's time to embrace our humanity, our agency, our creativity, and our collective fraternity.

It's time to be a Techno-Humanist.

It's time to demand better.
