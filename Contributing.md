I have not decided on any particular license or creative commons attribution (yet). If you have a suggestion, please open a MR with your reasons.

In the meantime, you may fork, copy, reuse, or whatever, with or without attribution. However, I'd prefer it if you created an issue (or an MR) with your suggestions.
